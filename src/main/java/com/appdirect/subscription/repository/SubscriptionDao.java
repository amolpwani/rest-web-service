package com.appdirect.subscription.repository;

import java.net.UnknownHostException;

import com.appdirect.subscription.domain.SubscriptionResponse;

public interface SubscriptionDao {
	
	void intialize() throws UnknownHostException;
	
	SubscriptionResponse save(final String subscriptionJson);
	
	SubscriptionResponse update(final String subscriptionJson);

}
