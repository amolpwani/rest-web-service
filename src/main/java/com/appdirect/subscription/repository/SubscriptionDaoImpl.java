package com.appdirect.subscription.repository;

import java.net.UnknownHostException;

import org.apache.commons.lang.StringUtils;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appdirect.subscription.domain.SubscriptionResponse;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.util.JSON;

public class SubscriptionDaoImpl implements SubscriptionDao  {

	private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionDaoImpl.class);
	
	private static final String ACCOUNT_IDENTIFIER = "accountIdentifier";
	private static final String ACCOUNT = "account";
	private static final String PAYLOAD = "payload";
	private static final String ERR_DB_CONNECTION_FAILED = "Err_01";
	private static final String ERR_ACCOUNT_IDENTIFIER_NOT_FOUND = "Err_02";

	private MongoClient mongo;
	private DB db;
	private DBCollection collection;
	private boolean dbConnectionSuccess = false;
	private String accountIdentifier;

	@Override
	public void intialize() throws UnknownHostException {
		LOGGER.debug("DB connection started....");

		mongo = new MongoClient("localhost", 27017);
		try {
			mongo.getDatabaseNames();
			db = mongo.getDB("app-direct-db");
			collection = db.getCollection("subscription");
			LOGGER.debug("DB connection successful....");
			dbConnectionSuccess = true;
		} catch (MongoException e) {
			LOGGER.error("DB connection failed....");
			dbConnectionSuccess = false;
		}
	}

	public DBCollection getSubscription() {
		return collection;
	}

	@Override
	public SubscriptionResponse save(String subscriptionJson) {
		// convert JSON to DBObject directly

		SubscriptionResponse response = new  SubscriptionResponse();

		if (dbConnectionSuccess) {
			DBObject dbObject = (DBObject) JSON.parse(subscriptionJson);
			collection.save(dbObject);
			final String accountIdentifier = dbObject.get("_id").toString();
			LOGGER.debug("Subscription inserted successfully....");
			response.setSuccess(true);
			response.setAccountIdentifier(accountIdentifier);
			response.setErrorCode(null);
			response.setMessage("Subscription ordered.");
			response = getSubscriptionResponse(true, 
											   null, 
											   "Subscription ordered.",
											   accountIdentifier);
			
		} else {
			response = getSubscriptionResponse(false, 
									ERR_DB_CONNECTION_FAILED, 
								    "Subscription not ordered due to db connection failed.",
								    null);
		}

		return response;
	}

	@Override
	public SubscriptionResponse update(String subscriptionJson) {
		SubscriptionResponse response = new  SubscriptionResponse();
		
		if (dbConnectionSuccess) {
			DBObject subscriptionObject = (DBObject) JSON.parse(subscriptionJson);
			boolean	accountIdentifierFound = isAccountIndentifierFound(subscriptionObject);
			
			if (!accountIdentifierFound) {
				return getSubscriptionResponse(false, 
											   ERR_ACCOUNT_IDENTIFIER_NOT_FOUND, 
											   "Subscription not found",
											   null);
			}
			
			try {
				DBObject searchById = new BasicDBObject("_id", new ObjectId(accountIdentifier));
				DBObject found = collection.findOne(searchById);
				
				if (found != null) {
					collection.findAndModify(searchById, subscriptionObject);
					
					response = getSubscriptionResponse(true, 
													   null, 
													   "Subscription cancelled/updated successfully.",
													   null);
											
				} else {
					response = getSubscriptionResponse(false, 
													   ERR_ACCOUNT_IDENTIFIER_NOT_FOUND, 
													   "Subscription not found",
													   null);
				}
			} catch (IllegalArgumentException e) {
				response = getSubscriptionResponse(false, 
												   ERR_ACCOUNT_IDENTIFIER_NOT_FOUND, 
												   "Subscription not found",
												   null);
			}
			
		} else {
			response = getSubscriptionResponse(false, 
											   ERR_DB_CONNECTION_FAILED, 
											   "Subscription not found",
											   null);
		}

		return response;
	}

	private SubscriptionResponse getSubscriptionResponse(boolean success, String errorCode, String message, String accountIndentifier) {
		SubscriptionResponse response = new  SubscriptionResponse();

		response.setSuccess(success);
		response.setErrorCode(errorCode);
		response.setMessage(message);
		if (StringUtils.isNotEmpty(accountIndentifier)) {
			response.setAccountIdentifier(accountIndentifier);
		}
		
		return response;
	}
	
	private boolean isAccountIndentifierFound(DBObject subscriptionObject) {
		boolean accountIdentifierFound = true;
		
		DBObject payload = (DBObject)subscriptionObject.get(PAYLOAD);
		DBObject account = null;
		if (payload != null) {
			account = (DBObject)payload.get(ACCOUNT);
			if (account != null) {
				accountIdentifier = account.get(ACCOUNT_IDENTIFIER).toString();
			} else {
				accountIdentifierFound = false;
			}
		} else {
			accountIdentifierFound = false;
		}
		
		return accountIdentifierFound;
	}

}
