package com.appdirect.subscription.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.appdirect.subscription.domain.SubscriptionResponse;
import com.appdirect.subscription.repository.SubscriptionDaoImpl;

@Service
@Validated
public class SubscriptionServiceImpl implements SubscriptionService {

    private final SubscriptionDaoImpl repository;

	@Autowired
    public SubscriptionServiceImpl(final SubscriptionDaoImpl repository) {
        this.repository = repository;
    }

    @Override
    public SubscriptionResponse save(final String subscription, final String requestType) {
    	return repository.save(subscription);
    }

	@Override
	public SubscriptionResponse update(String subscriptionJson, String type) {
		return repository.update(subscriptionJson);
	}
}
