package com.appdirect.subscription.service;

import com.appdirect.subscription.domain.SubscriptionResponse;

public interface SubscriptionService {

	SubscriptionResponse save(final String subscriptionJson, final String type);

	SubscriptionResponse update(final String subscriptionJson, final String type);

}
