package com.appdirect.subscription.service.exception;

@SuppressWarnings("serial")
public class SubscriptionAlreadyExistsException extends RuntimeException {

    public SubscriptionAlreadyExistsException(final String message) {
        super(message);
    }
}
