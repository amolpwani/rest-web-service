package com.appdirect.subscription.resource;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.appdirect.subscription.domain.SubscriptionResponse;
import com.appdirect.subscription.service.SubscriptionService;

@RestController
public class SubscriptionResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionResource.class);
	private final SubscriptionService subscriptionService;

	@Autowired
	public SubscriptionResource(final SubscriptionService subscriptionService) {
		this.subscriptionService = subscriptionService;
	}

	@RequestMapping(value = "/subscription", method = RequestMethod.POST)
	public ResponseEntity<SubscriptionResponse> createSubscription(@RequestParam(value = "type", required = true) String requestType, 
			@RequestBody @Valid final String subscription) {
		LOGGER.debug("Received request to create the {}", subscription);
		SubscriptionResponse response = subscriptionService.save(subscription, requestType);
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	@RequestMapping(value = "/subscription", method = RequestMethod.PUT)
	public ResponseEntity<SubscriptionResponse> cancelSubscription(@RequestParam(value = "type", required = true) String requestType, 
			@RequestBody @Valid final String subscription) {
		LOGGER.debug("Received request to create the {}", subscription);
		SubscriptionResponse response = subscriptionService.update(subscription, requestType);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

}
