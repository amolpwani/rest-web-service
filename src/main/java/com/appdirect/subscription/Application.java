package com.appdirect.subscription;

import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.appdirect.subscription.repository.SubscriptionDao;
import com.appdirect.subscription.repository.SubscriptionDaoImpl;

@SpringBootApplication
@Configuration
public class Application {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		ConfigurableApplicationContext app = SpringApplication.run(Application.class, args);
		SubscriptionDao dbConnection = (SubscriptionDao) app.getBean("dbService");
		
		try {
			dbConnection.intialize();
		} catch (UnknownHostException e) {
			LOGGER.error("DB connection failed.");
		}
	}
	
	@Bean(name = "dbService")
	public SubscriptionDaoImpl subscriptionRepository() {
		return new SubscriptionDaoImpl();
	}
}

