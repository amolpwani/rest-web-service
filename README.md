Spring Boot REST Service for AppDirect
================================

Requirements
------------
* [Java Platform (JDK) 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* [Apache Maven 3.x](http://maven.apache.org/)
* [MongoDb 3.2](https://docs.mongodb.com/manual/installation/)

Quick start
-----------
1. `mvn package`

2. `java -jar target/app-direct-test-1.0-SNAPSHOT.jar`

3. `Point your browser to [http://localhost:8080](http://localhost:8080)`

4. `This will generate Authorization to access rest service calls`
	curl -X POST -vu clientapp:123456 http://localhost:8080/oauth/token -H "Accept: application/json" -d 	"password=spring&username=roy&grant_type=password&scope=read%20write&client_secret=123456&client_id=clientapp"
		
5. `For creating subscription order we can use :`

	curl -X POST -d '{
		 "type": "SUBSCRIPTION_ORDER",
	    "marketplace": {
	      "baseUrl": "https://www.acme.com",
	      "partner": "APPDIRECT"
	    },
	    "creator": {
	      "address": {
	        "firstName": "Test",
	        "fullName": "Test User",
	        "lastName": "User"
	      },
	      "email": "testuser@testco.com",
	      "firstName": "Test",
	      "language": "en",
	      "lastName": " User",
	      "openId": "https://www.acme.com/openid/id/47cb8f55-1af6-5bfc-9a7d-8061d3aa0c97",
	      "uuid": "47cb8f55-1af6-5bfc-9a7d-8061d3aa0c97"
	    },
	    "payload": {
	      "company": {
	        "country": "US",
	        "name": "tester",
	        "phoneNumber": "1-800-333-3333",
	        "uuid": "385beb51-51ae-4ffe-8c05-3f35a9f99825",
	        "website": "www.testco.com"
	      },
	      "order": {
	        "editionCode": "Standard",
	        "pricingDuration": "MONTHLY",
	        "item": {
	          "quantity": "4",
	          "unit": "USER"
	        }
	      }
	    }
	}' http://localhost:8080/subscription?type=SUBSCRIPTION_ORDER

6. `For Canceling or updating existing subscription order we can use`
	curl -X PUT -d '{
		"type": "SUBSCRIPTION_CANCEL",
	    "marketplace": {
	      "baseUrl": "https://www.acme.com",
	      "partner": "APPDIRECT"
	    },
	    "creator": {
	      "address": {
	        "firstName": "Rahul",
	        "fullName": "Test User",
	        "lastName": "Wani"
	      },
	      "email": "testuser@testco.com",
	      "firstName": "Test",
	      "language": "en",
	      "lastName": " User",
	      "openId": "https://www.acme.com/openid/id/47cb8f55-1af6-5bfc-9a7d-8061d3aa0c97",
	      "uuid": "47cb8f55-1af6-5bfc-9a7d-8061d3aa0c97"
	    },
	    "payload": {
	            "account": {
		        "accountIdentifier": "57e9fc037f42fd182ab437b",
		        "status": "ACTIVE"
	      }
	    }
	}' http://localhost:8080/subscription?type=SUBSCRIPTION_CANCEL

7. `We can also use Postman which is pretty simple for calling rest service calls.`